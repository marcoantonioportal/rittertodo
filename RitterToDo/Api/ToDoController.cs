﻿using Moo;
using RitterToDo.Core;
using RitterToDo.Models;
using RitterToDo.Repos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace RitterToDo.Api
{
    public class ToDoController : ApiController
    {

        public IRepository<ToDo> ToDoRepo { get; private set; }

        public ILookupHelper<ToDoCategory, ToDoCategoryViewModel> CategoryHelper { get; private set; }

        public IMappingRepository MappingRepository { get; private set; }

        

        public ToDoController(
            IRepository<ToDo> todoRepo,
            ILookupHelper<ToDoCategory, ToDoCategoryViewModel> categoryHelper,
            IMappingRepository mappingRepository)
        {
            ToDoRepo = todoRepo;
            CategoryHelper = categoryHelper;
            MappingRepository = mappingRepository;
        }





        // GET api/<controller>
        public IEnumerable<ToDoViewModel> Get()
        {
            return new ToDoViewModel[]
            {
                new ToDoViewModel() { CategoryName = "General", Description = "Dummy desc", Name = "Dummy name" },
            };
        }

        // GET api/<controller>/5
        
        
        public string Get(Guid id)
        {

            var mapper = MappingRepository.ResolveMapper<ToDo, ToDoViewModel>();
            var model = mapper.Map(ToDoRepo.GetById(id));
            
            return "model";


        }
        /*
        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
         * */
    }
}